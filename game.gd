tool
extends Node2D

var goals = 0
var score = 0
var text = "Crates: %d / %d"

func set_goal(change=0):
	goals += change
	get_node("score").set_text(text % [score, goals])

func score_goal(change=0):
	score += change
	get_node("score").set_text(text % [score, goals])

func _ready():
	set_goal()