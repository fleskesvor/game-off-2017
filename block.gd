tool
extends KinematicBody2D

export(Texture) var texture setget set_texture, get_texture

const tile_width = 64
var directions = {
	"left": -Vector2(tile_width, 0),
	"right": Vector2(tile_width, 0),
	"up": -Vector2(0, tile_width),
	"down": Vector2(0, tile_width)
}

func set_texture(value):
	if has_node("Sprite"):
		get_node("Sprite").set_texture(value)

func get_texture():
	if has_node("Sprite"):
		return get_node("Sprite").get_texture()

func _get(property):
	# TODO: Needs improvement
	if property == "transform/pos":
		var offset = tile_width / 2
		var x = get_pos().x / tile_width
		var y = get_pos().y / tile_width
		set_pos(Vector2(round(x) * tile_width - offset, round(y) * tile_width - offset))

func _ready():
	for direction in directions.keys():
		get_node(direction).add_exception(self)
