tool
extends "res://block.gd"

func adjust_position():
	# TODO: There's a function test_move() which can probably replace this
	var offset = tile_width / 2
	var x = get_pos().x / offset
	var y = get_pos().y / offset
	set_pos(Vector2(round(x) * offset, round(y) * offset))

func _input(event):
	for direction in directions.keys():
		if event.is_action_pressed("ui_%s" % direction):
			var raycast = get_node(direction)
			var collider = raycast.get_collider()

			if collider and raycast.is_colliding() and collider.get_name().find("crate") > -1:
				if collider.move_crate(direction):
					# Toggle position off+on to enable move on same turn
					set_shape_as_trigger(0, true)
					move_to(get_pos() + directions[direction])
					set_shape_as_trigger(0, false)
			else:
				move_to(get_pos() + directions[direction])

	if not event.is_pressed():
		adjust_position()

func _ready():
	set_process_input(true)
