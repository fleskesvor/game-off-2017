tool
extends Area2D

const tile_width = 64

func _get(property):
	# TODO: Needs improvement
	if property == "transform/pos":
		var offset = tile_width / 2
		var x = get_pos().x / tile_width
		var y = get_pos().y / tile_width
		set_pos(Vector2(round(x) * tile_width - offset, round(y) * tile_width - offset))

func _enter_tree():
	get_parent().set_goal(1)

func _exit_tree():
	get_parent().set_goal(-1)

func _on_goal_body_enter(body):
	# TODO: Check if body is crate
	get_parent().score_goal(1)

func _on_goal_body_exit(body):
	# TODO: Check if body is crate
	get_parent().score_goal(-1)
