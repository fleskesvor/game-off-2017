tool
extends "res://block.gd"

func move_crate(direction):
	var raycast = get_node(direction)
	var collider = raycast.get_collider()
	if collider and raycast.is_colliding():
		return false

	move_to(get_pos() + directions[direction])
	return true
