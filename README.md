Game Off 2017
=============

Concept
-------

A mix of Snake and Sokoban with focus on solving simple puzzles. Crates can be pushed on top of buttons to keep doors open or into holes to fill gaps. The snake grows when eating fruit, which will enable it to cross some gaps or to hold a button with its tail while going through a door. 

Goals
-----

My goal is to get more experience with making a simple game with [Godot](https://godotengine.org/) from the ground up, and get better acquainted with concept like input handling, tilemaps and collision detection. And most of all, I intend to have fun creating something.

Misc
----

I initially considered the name Snakoban for the game, but when googling it, I realized it had already been taken. Multiple times.
